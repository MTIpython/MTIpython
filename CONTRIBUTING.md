Development rules MTIpython
===

Development needs to be done according to the following guidelines:
* A commit needs to WORK!
* Use the issue system! Feature request and bug fixes are to be reported using the templates
* All public accessible code needs to be documented with Sphinx
* The working of MTIpython is to be guaranteed  are to be unit tested
* Unit safe functions, using Pint
* Source citation, all functions and properties need to have a reference using **mtibib**
* Latex output for output functions using **mtiprint**
* When fixing a bug or adding a feature a WIP merge request needs to be performed
* Merge request are only allowed when all tests are PASSED and all discussions are resolved
* A merge request is always reviewed by somebody else