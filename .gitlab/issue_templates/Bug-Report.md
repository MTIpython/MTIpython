Bug report
===

When you encounter a bug, try running your code with logging enabled.
```python
from MTIpython.core.logging import setup_logging
setup_logging()

... your code ...
```
Attach the generated log files with your issue

## What was the problem?
enter text

## Which version or commit?
enter text

## Which OS (Windows 7, 10, Linux)?
Windows 7

## Description:
### Steps to reproduce:
enter text

### Expected result:
enter text

### Actual result:
enter text

/label ~"bug"