Merge request
===
Please do not create a Merge Request without creating an issue first. Any change needs to be discussed before proceeding. Failure to do so may result in the rejection of the merge request.

**Please check if the MR fulfils these requirements**
- [ ] The commit message follows our guidelines set out in [CONTRIBUTING.md](http://mti-gitlab.ihc.eu/MTIpython/MTIpython/blob/develop/CONTRIBUTING.md)
- [ ] Tests for the changes have been added (for bug fixes / features)
- [ ] Docs have been added / updated (for bug fixes / features)

## What kind of change does this MR introduce? (Bug fix, feature, docs update, ...)
enter text

## What is the current behaviour? (You can also link to an open issue here)
enter text

## What is the new behaviour (if this is a feature change)?**
enter text

## Does this MR introduce a breaking change? (What changes might users need to make in their application due to this MR?)
enter text

## Are there new third party modules introduced?
When a new thirdparty module is required, make sure that you add it to the [setup.py](http://mti-gitlab.ihc.eu/MTIpython/MTIpython/blob/develop/setup.py) file `install_requires` list.
Afterwards execute the `pip-compile --upgrade` command such that the requirements.txt are updated.

## Other information:
enter text

## How much time did you worked on it?
/spend 