from MTIpython.mtimath.geometry import *
from MTIpython.units import *

import unittest


class TestMath(unittest.TestCase):
    def test_A_circle_function(self):
        D = 100 * u['mm']
        A = A_circle(D=D)
        expected_value = 0.007853981633974483 * u.m ** 2
        self.assertEqual(A, expected_value)
        r = 50 * u['mm']
        A = A_circle(r=r)
        expected_value = 0.007853981633974483 * u.m ** 2
        self.assertEqual(A, expected_value)
