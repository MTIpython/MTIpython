import os
import unittest

import numpy as np

from pkg_resources import resource_listdir

from MTIpython.core import logging
from MTIpython.core.binder import Binder, Function
from MTIpython.core.exception import *
from MTIpython.core.functions import LUT
from MTIpython.core.logging import logged
from MTIpython.material import *
from MTIpython.material.principal.core import *
from MTIpython.material.granular import ParticleSizeDistribution
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import *

logging.setup_logging(default_level=logging.DEBUG)


class MaterialTestCase(unittest.TestCase):
    def tearDown(self):
        if os.path.exists('material.tst'):
            os.remove('material.tst')


@logged
class TestMaterial(MaterialTestCase):
    def test_rho(self):
        mat = Matter(name='Test Matter', rho=1000. * u.kg / u.m ** 3, T=15. * u.degC, c_p=255. * u.J / (u.kg * u.K))
        mat.rho = 200. * u.kg / u.l
        self.assertEqual(200.e3 * u.kg / u.m ** 3, round(mat.rho, 3))
        with self.assertRaises(ValueError):
            mat.rho = -1000. * u.kg / u.m ** 3

        with self.assertRaises(DimensionalityError):
            mat.rho = 1000. * u.m / u.s

        mat.save('material.tst')

        mat2 = Matter()
        mat2.load('material.tst')
        mat2.rho = 200. * u.kg / u.l
        self.assertEqual(200e3 * u.kg / u.m ** 3, round(mat.rho, 3))
        with self.assertRaises(ValueError):
            mat2.rho = -1000. * u.kg / u.m ** 3

        with self.assertRaises(DimensionalityError):
            mat2.rho = 1000. * u.m / u.s

    def test_rho_func(self):
        def rho_func(T):
            return 2. * u.kg / (u.K * u.m ** 3) * T

        mat = Matter(name='Test Matter', T=15. * u.degC, c_p=255. * u.J / (u.kg * u.K),
                     rho=Binder(value=1000. * u.kg / u.m ** 3, bounds=(0., inf),
                                func1=Function(rho_func, T='T')))
        self.assertEqual(576.3 * u.kg / u.m ** 3, mat.rho)
        mat.T = 200 * u.degC
        self.assertEqual(946.3 * u.kg / u.m ** 3, mat.rho)
        mat.save('material.tst')

        mat2 = Matter()
        mat2.load('material.tst')
        mat2.T = 15. * u.degC
        self.assertEqual(576.3 * u.kg / u.m ** 3, mat2.rho)
        mat2.T = 200 * u.degC
        self.assertEqual(946.3 * u.kg / u.m ** 3, mat2.rho)

    def test_rho_lut(self):
        mat = Matter(name='Test Matter', T=15. * u.degC, c_p=255. * u.J / (u.kg * u.K),
                     rho=Binder(unit=u.kg / u.m ** 3,
                                func1=Function(LUT(T=[0., 100., 500., 1000.] * u.K,
                                                   rho=[500., 750., 800., 850.] * u.kg / u.m ** 3),
                                               T='T')))
        self.assertEqual(773.51875 * u.kg / u.m ** 3, mat.rho)
        mat.T = 800. * u.K
        self.assertEqual(830. * u.kg / u.m ** 3, mat.rho)


class TestLiquid(MaterialTestCase):
    def testFluid(self):
        liquid = Liquid(name='Liquid', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC, P=1. * u.bar, mu=0.01 * u.Pa * u.s)
        liquid.save('material.tst')

        liquid2 = Liquid()
        liquid2.load('material.tst')

        self.assertEqual(repr(liquid), repr(liquid2))


class TestBingham(MaterialTestCase):
    def testBingham(self):
        bingham = Bingham(name='Bingham', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC, P=1. * u.bar,
                          eta=2.42 * u.Pa * u.s, tau=100. * u.Pa)
        bingham.save('material.tst')

        bingham2 = Bingham()
        bingham2.load('material.tst')

        self.assertEqual(repr(bingham), repr(bingham2))


class TestGas(MaterialTestCase):
    def testGas(self):
        gas = Gas(name='Gas', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC, p=1. * u.bar,
                  mu=0.01 * u.Pa * u.s)
        gas.save('material.tst')

        gas2 = Gas()
        gas2.load('material.tst')

        self.assertEqual(repr(gas), repr(gas2))


class TestSolid(MaterialTestCase):
    def testSolid(self):
        solid = Solid(name='Solid', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC, P=1. * u.bar,
                      mu=0.01 * u.Pa * u.s)
        solid.save('material.tst')

        solid2 = Solid()
        solid2.load('material.tst')

        self.assertEqual(repr(solid), repr(solid2))


class TestMetal(MaterialTestCase):
    def testMetal(self):
        metal = Metal(name='Metal', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC, P=1. * u.bar,
                      mu=0.01 * u.Pa * u.s)
        metal.save('material.tst')

        metal2 = Metal()
        metal2.load('material.tst')

        self.assertEqual(repr(metal), repr(metal2))


class TestPlastic(MaterialTestCase):
    def testPlastic(self):
        plastic = Plastic(name='Plastic', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC, P=1. * u.bar,
                          mu=0.01 * u.Pa * u.s)
        plastic.save('material.tst')

        plastic2 = Plastic()
        plastic2.load('material.tst')

        self.assertEqual(repr(plastic), repr(plastic2))


class TestGranular(MaterialTestCase):
    def testGranular(self):
        base_mat = Matter('Slag', rho=800. * u.kg / u.m ** 3, T=15. * u.degC)
        granular = Granular(name='Granular', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC, P=1. * u.bar,
                            mu=0.01 * u.Pa * u.s, basematerial=base_mat)
        granular.save('material.tst')

        granular2 = Granular()
        granular2.load('material.tst')

        self.assertEqual(repr(granular), repr(granular2))


class TestPowder(MaterialTestCase):
    def testPowder(self):
        base_mat = Matter('Slag', rho=800. * u.kg / u.m ** 3, T=15. * u.degC)
        powder = Powder(name='Powder', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC, P=1. * u.bar,
                        mu=0.01 * u.Pa * u.s, Lambda=3.0, basematerial=base_mat)
        powder.save('material.tst')

        powder2 = Powder()
        powder2.load('material.tst')

        self.assertEqual(repr(powder), repr(powder2))


class Testcore(MaterialTestCase):
    def test_kinematic_viscosity(self):
        rho = 1000. * u.kg / u.m ** 3
        mu = 0.002 * u.Pa * u.s
        nu = 2e-06 * u.m ** 2 / u.s
        self.assertEqual(nu, kinematic_viscosity(rho=rho, mu=mu).to(u.m ** 2 / u.s))

    def test_dynamic_viscosity(self):
        rho = 1000. * u.kg / u.m ** 3
        mu = 0.002 * u.Pa * u.s
        nu = 2e-06 * u.m ** 2 / u.s
        self.assertEqual(mu, dynamic_viscosity(rho=rho, nu=nu).to(u.Pa * u.s))

    def test_specific_volume(self):
        rho = 1000. * u.kg / u.m ** 3
        v = 0.001 * u.m ** 3 / u.kg
        m = 1000. * u.kg
        V = 1. * u.m ** 3
        self.assertEqual(v, specific_volume(rho=rho).to(u.m ** 3 / u.kg))
        self.assertEqual(v, specific_volume(m=m, V=V).to(u.m ** 3 / u.kg))

    def test_specific_weight(self):
        rho = 1000. * u.kg / u.m ** 3
        gamma = 9806.65 * u.N / u.m ** 3
        v = 0.001 * u.m ** 3 / u.kg
        m = 1000. * u.kg
        V = 1. * u.m ** 3
        self.assertEqual(gamma, specific_weight(rho=rho).to(u.N / u.m ** 3))
        self.assertEqual(gamma, specific_weight(v=v).to(u.N / u.m ** 3))
        self.assertEqual(gamma, specific_weight(m=m, V=V).to(u.N / u.m ** 3))

    def test_density(self):
        rho = 1000. * u.kg / u.m ** 3
        gamma = 9806.65 * u.N / u.m ** 3
        v = 0.001 * u.m ** 3 / u.kg
        m = 1000. * u.kg
        V = 1. * u.m ** 3
        self.assertEqual(rho, density(gamma=gamma).to(u.kg / u.m ** 3))
        self.assertEqual(rho, density(v=v).to(u.kg / u.m ** 3))
        self.assertEqual(rho, density(m=m, V=V).to(u.kg / u.m ** 3))

    def test_mass(self):
        rho = 1000. * u.kg / u.m ** 3
        gamma = 9806.65 * u.N / u.m ** 3
        v = 0.001 * u.m ** 3 / u.kg
        m = 1000. * u.kg
        V = 1. * u.m ** 3
        self.assertEqual(m, mass(rho=rho, V=V).to('kg'))
        self.assertEqual(m, mass(gamma=gamma, V=V).to('kg'))
        self.assertEqual(m, mass(v=v, V=V).to('kg'))

    def test_volume(self):
        rho = 1000. * u.kg / u.m ** 3
        gamma = 9806.65 * u.N / u.m ** 3
        v = 0.001 * u.m ** 3 / u.kg
        m = 1000. * u.kg
        V = 1. * u.m ** 3
        self.assertEqual(V, volume(rho=rho, m=m).to('m**3'))
        self.assertEqual(V, volume(gamma=gamma, m=m).to('m**3'))
        self.assertEqual(V, volume(v=v, m=m).to('m**3'))


class TestMaterialFactory(MaterialTestCase):
    def test_list(self):
        resources_lst = resource_listdir('MTIpython', 'resources/materials/')
        resources_lst.remove('data')
        self.assertEqual(len(resources_lst) + 1,
                         len(material_factory.list.keys()))
        self.assertEqual(len(resource_listdir('MTIpython', 'resources/materials/' +
                                              resource_listdir('MTIpython', 'resources/materials/')[3])),
                         len(material_factory.list[resource_listdir('MTIpython', 'resources/materials/')[3]]))

    def test_material_load(self):
        water = material_factory['water']
        self.assertEqual(water.name, 'Water')

    def test_material_load_keyerror(self):
        with self.assertRaises(KeyError) as context:
            beer = material_factory['beer']

        self.assertTrue('Material beer not found in package resources!' in context.exception.args[0])


class TestParticleSizeDistribution(MaterialTestCase):
    def test_call_array(self):
        psd = ParticleSizeDistribution(
            size=np.array([0.325, 0.55, 1.1, 2.1]) * u.mm,
            fraction=np.array([0., 0.25, 0.75, 1.])
        )
        np.testing.assert_equal(psd().m, np.array([0.325, 0.55, 1.1, 2.1]))
        self.assertAlmostEqual(psd(fraction=0.5), 0.825 * u.mm, 6)
        np.testing.assert_almost_equal(psd(fraction=np.array([0.12, 0.5])).m, np.array([0.433, 0.825]), 6)
        self.assertAlmostEqual(psd(size=0.825 * u.mm), 0.5 * u.dimensionless, 6)
        np.testing.assert_almost_equal(psd(size=np.array([0.433, 0.825]) * u.mm).m, np.array([0.12, 0.5]), 6)

    def test_call_magnitude(self):
        psd = ParticleSizeDistribution(size=2. * u.mm)
        self.assertEqual(psd(), 2. * u.mm)
        self.assertEqual(psd(fraction=0.2), 2. * u.mm)
