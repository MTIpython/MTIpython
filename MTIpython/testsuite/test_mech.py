import unittest

import numpy as np

from MTIpython.material import Matter
from MTIpython.mech.auger import *
from MTIpython.mech.principal import *
from MTIpython.mech.gear import *
from MTIpython.units.SI import u


class TestAuger(unittest.TestCase):
    def setUp(self):
        base_mat = Matter('Slag', rho=800. * u.kg / u.m ** 3, T=15. * u.degC)
        self.powder = Powder(name='Slag', rho=700. * u.kg / u.m ** 3, T=15. * u.degC, Lambda=3., basematerial=base_mat)
        self.auger = Auger(medium=self.powder, D=50. * u.mm, L=2. * u.m, S=200. * u.mm, phi=Filling.HEAVY,
                           H=-100. * u.mm)

    def test_v_to_P(self):
        self.auger.v = 1. * u.m / u.s
        self.assertEqual(0.016928665937452608 * u.kW, self.auger.P.to('kW'))

    def test_m_flux_to_P(self):
        self.auger.m_flux = 2. * u.kg / u.s
        self.assertEqual(2910.2618165375147 * u.rpm, self.auger.n)
        self.assertEqual(0.12071847000000002 * u.kW, self.auger.P.to('kW'))


class TestCore(unittest.TestCase):
    def test_application_factor(self):
        self.assertEqual(1.5, core.application_factor(driven_machine=core.WorkingCharacteristic.LIGHT_SHOCKS,
                                                      driving_machine=core.WorkingCharacteristic.MODERATE_SHOCKS))
        self.assertEqual(2.25, core.application_factor(driven_machine=core.WorkingCharacteristic.HEAVY_SHOCKS,
                                                       driving_machine=core.WorkingCharacteristic.HEAVY_SHOCKS))

    def test_stress(self):
        self.assertEqual(50000.0 * u.N / u.m ** 2, core.stress(F_t=10. * u.N, A=200. * u.mm ** 2).to('N/m**2'))
        self.assertEqual(50000.0 * u.N / u.m ** 2, core.stress(F_c=10. * u.N, A=200. * u.mm ** 2).to('N/m**2'))
        self.assertEqual(50000.0 * u.N / u.m ** 2, core.stress(F_s=10. * u.N, A=200. * u.mm ** 2).to('N/m**2'))
        self.assertEqual(50000.0 * u.N / u.m ** 2, core.stress.shear(F_s=10. * u.N, A=200. * u.mm ** 2).to('N/m**2'))
        self.assertEqual(50000000.0 * u.N / u.m ** 2,
                         core.stress(M_b=10. * u.N * u.m, S_x=200. * u.mm ** 3).to('N/m**2'))
        self.assertEqual(50000000.0 * u.N / u.m ** 2,
                         core.stress.bending(M_b=10. * u.N * u.m, S_x=200. * u.mm ** 3).to('N/m**2'))
        self.assertEqual(50000000.0 * u.N / u.m ** 2,
                         core.stress(M_b=10. * u.N * u.m, I_x=400. * u.mm ** 4, e=2. * u.mm).to('N/m**2'))
        self.assertEqual(50000000.0 * u.N / u.m ** 2,
                         core.stress.bending(M_b=10. * u.N * u.m, I_x=400. * u.mm ** 4, e=2. * u.mm).to('N/m**2'))
        self.assertEqual(50000000.0 * u.N / u.m ** 2,
                         core.stress(T=10. * u.N * u.m, S_t=200. * u.mm ** 3).to('N/m**2'))
        # TODO add traverse shear test

    def test_power(self):
        self.assertEqual(200. * u.W, core.power(T=10. * u.N * u.m, omega=20. * u.rad / u.s).to('W'))
        self.assertEqual(200. * u.W, core.power(F=10. * u.N, v=20. * u.m / u.s).to('W'))

    def test_torque(self):
        self.assertEqual(10. * u.N * u.m, core.torque(P=200. * u.W, omega=20. * u.rad / u.s).to('N*m'))

    def test_allowable_stress(self):
        self.assertEqual(133.33333333333334 * u.MPa, core.allowable_stress(R_eN=200. * u.MPa, S_V_min=1.5))
        self.assertEqual(133.33333333333334 * u.MPa, core.allowable_stress.static(R_eN=200. * u.MPa, S_V_min=1.5))
        self.assertEqual(100. * u.MPa, core.allowable_stress(R_mN=200. * u.MPa, S_B_min=2.))
        self.assertEqual(100. * u.MPa, core.allowable_stress.static(R_mN=200. * u.MPa, S_B_min=2.))
        self.assertEqual(57.142857142857146 * u.MPa, core.allowable_stress(sigma_D=200. * u.MPa, S_D_min=3.5))
        self.assertEqual(57.142857142857146 * u.MPa, core.allowable_stress.dynamic(sigma_D=200. * u.MPa, S_D_min=3.5))
        self.assertEqual(57.142857142857146 * u.MPa, core.allowable_stress(tau_D=200. * u.MPa, S_D_min=3.5))
        self.assertEqual(57.142857142857146 * u.MPa, core.allowable_stress.dynamic(tau_D=200. * u.MPa, S_D_min=3.5))
        with self.assertRaises(ValueError):
            core.allowable_stress(R_eN=200. * u.MPa, S_V_min=0.5)
        with self.assertRaises(ValueError):
            core.allowable_stress(R_eN=200. * u.MPa, S_V_min=[0.5, 1.5])
        with self.assertRaises(ValueError):
            core.allowable_stress(R_mN=200. * u.MPa, S_B_min=0.5)
        with self.assertRaises(ValueError):
            core.allowable_stress(R_mN=200. * u.MPa, S_B_min=[0.5, 1.5])
        with self.assertRaises(ValueError):
            core.allowable_stress(sigma_D=200. * u.MPa, S_D_min=0.5)
        with self.assertRaises(ValueError):
            core.allowable_stress(sigma_D=200. * u.MPa, S_D_min=[0.5, 1.5])
        with self.assertRaises(ValueError):
            core.allowable_stress(tau_D=200. * u.MPa, S_D_min=0.5)
        with self.assertRaises(ValueError):
            core.allowable_stress(tau_D=200. * u.MPa, S_D_min=[0.5, 1.5])

    def test_yield_safety_factor(self):
        self.assertEqual(0.35355339059327373 * u.dimensionless,
                         core.yield_safety_factor(sigma_b_max=100. * u.MPa, sigma_bv=50. * u.MPa,
                                                  tau_t_max=100. * u.MPa, tau_tv=50. * u.MPa))
        self.assertEqual(0.35355339059327373 * u.dimensionless,
                         core.yield_safety_factor(sigma_b_a=100. * u.MPa, sigma_GA=50. * u.MPa,
                                                  tau_t_a=100. * u.MPa, tau_GA=50. * u.MPa))

    def test_equivalent_torque(self):
        self.assertEqual(140. * u.N * u.m, core.equivalent_torque(T_nom=100. * u.N * u.m, K_A=1.4))

    def test_equivalent_moment(self):
        self.assertEqual(21.666666666666668 * u.N * u.m,
                         core.equivalent_moment(M=20. * u.N * u.m, T=10. * u.N * u.m, sigma_b=200. * u.MPa,
                                                tau_t=120. * u.MPa))
        self.assertEqual(21.262251370288457 * u.N * u.m,
                         core.equivalent_moment(M=20. * u.N * u.m, T=10. * u.N * u.m, sigma_b=200. * u.MPa,
                                                tau_t=120. * u.MPa, phi=2))
        self.assertEqual(180. * u.N * u.m, core.equivalent_moment(T=120. * u.N * u.m, phi=1.5))

    def test_axle_diameter_prime(self):
        self.assertEqual(26.985817883459397 * u.mm,
                         axle.diameter_prime(M=100. * u.N * u.m, sigma_bD=200. * u.MPa).to('mm'))
        self.assertEqual(26.985817883459397 * u.mm,
                         axle.diameter_prime.full(M=100. * u.N * u.m, sigma_bD=200. * u.MPa).to('mm'))
        self.assertEqual(28.263717983200532 * u.mm,
                         axle.diameter_prime(M=100. * u.N * u.m, sigma_bD=200. * u.MPa, k=0.6).to('mm'))
        self.assertEqual(28.263717983200532 * u.mm,
                         axle.diameter_prime.hollow(M=100. * u.N * u.m, sigma_bD=200. * u.MPa, k=0.6).to('mm'))
        self.assertEqual(21.4299142015707 * u.mm,
                         axle.diameter_prime(T=100. * u.N * u.m, tau_tD=200. * u.MPa).to('mm'))
        self.assertEqual(21.4299142015707 * u.mm,
                         axle.diameter_prime.full(T=100. * u.N * u.m, tau_tD=200. * u.MPa).to('mm'))
        self.assertEqual(22.44471722195337 * u.mm,
                         axle.diameter_prime(T=100. * u.N * u.m, tau_tD=200. * u.MPa, k=0.6).to('mm'))
        self.assertEqual(22.44471722195337 * u.mm,
                         axle.diameter_prime.hollow(T=100. * u.N * u.m, tau_tD=200. * u.MPa, k=0.6).to('mm'))
        self.assertEqual(27.779518409443497 * u.mm,
                         axle.diameter_prime(T=100. * u.N * u.m, sigma_bD=200. * u.MPa, short_support=True).to('mm'))
        self.assertEqual(35.7165236692845 * u.mm,
                         axle.diameter_prime(T=100. * u.N * u.m, sigma_bD=200. * u.MPa, short_support=False).to('mm'))

    def test_axle_diameter(self):
        self.assertEqual(17.205080276561997 * u.mm, axle.diameter(M=100. * u.N * u.m, sigma_b=200. * u.MPa).to('mm'))
        self.assertEqual(17.205080276561997 * u.mm,
                         axle.diameter.full(M=100. * u.N * u.m, sigma_b=200. * u.MPa).to('mm'))
        self.assertEqual(18.019818369601193 * u.mm,
                         axle.diameter(M=100. * u.N * u.m, sigma_b=200. * u.MPa, k=0.6).to('mm'))
        self.assertEqual(18.019818369601193 * u.mm,
                         axle.diameter.hollow(M=100. * u.N * u.m, sigma_b=200. * u.MPa, k=0.6).to('mm'))
        self.assertEqual(13.655681265105917 * u.mm, axle.diameter(T=100. * u.N * u.m, tau_t=200. * u.MPa).to('mm'))
        self.assertEqual(13.655681265105917 * u.mm, axle.diameter.full(T=100. * u.N * u.m, tau_t=200. * u.MPa).to('mm'))
        self.assertEqual(14.302339318090409 * u.mm,
                         axle.diameter(T=100. * u.N * u.m, tau_t=200. * u.MPa, k=0.6).to('mm'))
        self.assertEqual(14.302339318090409 * u.mm,
                         axle.diameter.hollow(T=100. * u.N * u.m, tau_t=200. * u.MPa, k=0.6).to('mm'))
        self.assertEqual(12.0 * u.mm, axle.diameter(d_a=20. * u.mm, k=0.6).to('mm'))


class TestGear(unittest.TestCase):
    def setUp(self):
        self.gear_ratio = GearingRatio([2., 3.5, 120.])

    def test_gear_ratio_total(self):
        self.assertEqual(840., self.gear_ratio.total)
        self.assertEqual(420., self.gear_ratio.total[1:])

    def test_gear(self):
        np.testing.assert_array_equal([2., 3.5, 120.], self.gear_ratio)
        np.testing.assert_array_equal([3.5, 120.], self.gear_ratio[1:])
        np.testing.assert_array_equal([2.0, 120.], self.gear_ratio[0:3:2])
        self.assertEqual(3.5, self.gear_ratio[1])
