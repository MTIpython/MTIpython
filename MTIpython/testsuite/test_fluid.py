import unittest

from MTIpython.core import logging
from MTIpython.core.logging import logged
from MTIpython.fluid.principal.core import *
from MTIpython.fluid.principal.design import *
from MTIpython.material import Bingham, Liquid, material_factory
from MTIpython.units import *

logging.setup_logging(default_level=logging.DEBUG)


@logged
class TestPrincipalCore(unittest.TestCase):
    def setUp(self):
        self.liquid = Liquid(name='Water', rho=1000. * u.kg / u.m ** 3, T=20. * u.degC,
                             P=1. * u.bar,
                             nu=1.0035e-6 * u.m ** 2 / u.s, mu=0.0010016 * u.Pa * u.s)
        self.bingham = Bingham(name='Bingham', rho=2000. * u.kg / u.m ** 3, T=20. * u.degC,
                               P=1. * u.bar,
                               mu=0.58 * u.Pa * u.s, nu=0.00029 * u.m ** 2 / u.s, tau=100. * u.Pa,
                               eta=2.42 * u.Pa * u.s)

    def test_Reynold_Liquid(self):
        self.logger.info('Running test_Reynold_Liquid')
        self.assertEqual(59790732.43647235 * u.dimensionless,
                         Re(fluid=self.liquid, L=10. * u.m, v=6. * u.m / u.s))

    def test_Reynold_Bingham(self):
        self.logger.info('Running test_Reynold_Bingham')
        self.assertEqual(150.35975431412695 * u.dimensionless,
                         Re(fluid=self.bingham, d=0.031416 * u.m, v=6. * u.m / u.s))

    def test_flowregime(self):
        self.logger.info('Running test_flowregime')
        self.assertEqual(flowregime(10. * u['dimensionless']), FlowRegime.LAMINAR)
        self.assertEqual(flowregime(3000. * u['dimensionless']), FlowRegime.TRANSITIONAL)
        self.assertEqual(flowregime(10000. * u['dimensionless']), FlowRegime.TURBULENT)

    def test_velocity(self):
        self.logger.info('Running test_velocity')
        Q = 10. * u['m**3/s']
        A = 100 * u['mm**2']
        expected_value = 1e5 * u['m/s']
        self.assertEqual(velocity(Q=Q, A=A), expected_value)

    def test_colebrook_white(self):
        self.logger.info('Running test_colebrook_white')
        eps = 0.05 * u['mm']
        d = 100. * u['mm']
        Re = 8000.
        self.assertEqual(0.033472692797754861,
                         friction_factor(epsilon=eps, d=d, Re=Re, eqType='Colebrook-White'))
        # TODO fix-me see issue http://mti-gitlab.ihc.eu/MTIpython/fluid/issues/3
        # Re = np.arange(4000., 10000., 2000.) * u.dimensionless
        # np.testing.assert_array_almost_equal([0.04041032, 0.03610355, 0.03347269],
        # friction_factor(epsilon=eps, d=d, Re=Re, eqType='Colebrook-White'), 6)

    def test_swamee_jain(self):
        self.logger.info('Running test_swamee_jain')
        eps = 0.05 * u['mm']
        d = 100. * u['mm']
        Re = 8000.
        self.assertEqual(0.033746767023424558,
                         friction_factor(epsilon=eps, d=d, Re=Re, eqType='Swamee-Jain'))

    def test_haaland(self):
        self.logger.info('Running test_haaland')
        eps = 0.05 * u['mm']
        d = 100. * u['mm']
        Re = 8000.
        self.assertEqual(0.033409642281764358,
                         friction_factor(epsilon=eps, d=d, Re=Re, eqType='Haaland'))

    def test_friction_factor(self):
        self.logger.info('Running test_frictionfactor')
        eps = 0.05 * u['mm']
        d = 100. * u['mm']
        Re = 8000.
        expected_value = 0.033409642281764358
        self.assertEqual(friction_factor(Re=Re, d=d, epsilon=eps), expected_value)
        Re = 200.
        expected_value = 0.32
        self.assertEqual(friction_factor(Re=Re), expected_value)


@logged
class TestPrincipalParticle(unittest.TestCase):
    def setUp(self):
        self.air = material_factory['air_dry']

    def test_dimensionless_particle_size_grace(self):
        d = 60. * u.micrometer
        rho_s = 2000. * u.kg / u.m ** 3
        rho_f = self.air.rho
        mu = self.air.mu


@logged
class TestPrincipalDesign(unittest.TestCase):
    def setUp(self):
        self.water = material_factory['water']

    def test_design_diameter(self):
        d = pipe_diameter(L=30.5 * u.m, epsilon=4.572e-5 * u.m, h_l=1.402 * u.m, Q=0.014 * u.m ** 3/u.s,
                          nu=1.15e-5 * u.m ** 2 / u.s)
        self.assertAlmostEqual(d.to('mm'), 99.83987668372701 * u.mm,6)
        d = pipe_diameter(L=30.5 * u.m, epsilon=4.572e-5 * u.m, h_l=1.402 * u.m, Q=0.014 * u.m ** 3/u.s,
                          fluid=self.water)
        self.assertAlmostEqual(d.to('mm'), 93.71933482112476 * u.mm,6)


if __name__ == '__main__':
    unittest.main()
