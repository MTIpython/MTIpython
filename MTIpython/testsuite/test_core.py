from MTIpython import u
from MTIpython.core import logging
from MTIpython.core.binder import Binder, Function, SubscriptionList, Valid
from MTIpython.core.core import Product, rgetattr, rsetattr, set_printoptions
from MTIpython.core.exception import DimensionalityError, UndefinedUnitError
from MTIpython.core.functions import LUT, function_switch
from MTIpython.core.logging import logged
from MTIpython.core.mtiobj import MTIobj

logging.setup_logging(default_level=logging.DEBUG)
logging.getLogger('MTIpython.principal.binder.Valid').setLevel(logging.DEBUG)

import unittest
import numpy as np

set_printoptions()

def rho_from_mu_nu(mu, nu):
    return mu / nu


def rho_from_v(v):
    r"""
    Test documentation
    """
    return v ** -1


def rho_from_gamma(gamma):
    return gamma / u.g_0


def nu_from_mu_rho(mu, rho):
    return mu / rho


def mu_from_nu_rho(nu, rho):
    return nu * rho


def v_from_rho(rho):
    return rho ** -1


def v_from_gamma(gamma):
    return (gamma / u.g_0) ** -1


def gamma_from_rho(rho):
    return rho * u.g_0


def gamma_from_v(v):
    return v ** -1 * u.g_0


@logged
class SimpleClass(MTIobj):
    x = Valid()
    """:class:`SimpleClass.Valid` X attribute of Class SimpleClass"""

    y = Valid()
    """:class:`SimpleClass.Valid` Y attribute of Class SimpleClass"""

    z = Valid()
    """:class:`SimpleClass.Valid` Y attribute of Class SimpleClass"""

    def __init__(self, **kwargs):
        super(SimpleClass, self).__init__(**kwargs)

    def func(self, y_var):
        return 2. * y_var

    def func_inv(self, x_var):
        return x_var / 2.


@logged
class TestValid(unittest.TestCase):
    def test_simple_init(self):
        test = SimpleClass(x=Binder(value=1. * u.kg),
                           y=Binder(value=2. * u.m),
                           z=Binder(value=3. * u.s))
        self.assertEqual(1. * u.kg, test.x)
        self.assertEqual(2. * u.m, test.y)
        self.assertEqual(3. * u.s, test.z)

    def test_redefine_simple_binder(self):
        test = SimpleClass(x=Binder(value=1. * u.kg),
                           y=Binder(value=2. * u.m),
                           z=Binder(value=3. * u.s))
        test.x = Binder(value=4. * u.l)
        self.assertEqual(4. * u.l, test.x)
        self.assertEqual(2. * u.m, test.y)
        self.assertEqual(3. * u.s, test.z)

    def test_function_init(self):
        test = SimpleClass(x=Binder(value=1. * u.kg, func1=Function(SimpleClass.func, y_var='y')),
                           y=Binder(unit=u.kg, func1=Function(SimpleClass.func_inv, x_var='x')),
                           z=Binder(value=3. * u.s))
        self.assertEqual(1. * u.kg, test.x)
        self.assertEqual(0.5 * u.kg, test.y)
        self.assertEqual(3. * u.s, test.z)

    def test_redefine_function_alternate_value_change(self):
        test = SimpleClass(x=Binder(value=1. * u.kg, func1=Function(SimpleClass.func, y_var='y')),
                           y=Binder(unit=u.kg, func1=Function(SimpleClass.func_inv, x_var='x')),
                           z=Binder(value=3. * u.s))
        self.assertEqual(1. * u.kg, test.x)
        self.assertEqual(0.5 * u.kg, test.y)
        self.assertEqual(3. * u.s, test.z)
        test.y = 0.5 * u.kg
        self.assertEqual(1. * u.kg, test.x)
        self.assertEqual(0.5 * u.kg, test.y)
        self.assertEqual(3. * u.s, test.z)
        test.y = [0.5, 0.6] * u.kg
        np.testing.assert_array_equal([1., 1.2] * u.kg, test.x)
        np.testing.assert_array_equal([0.5, 0.6] * u.kg, test.y)
        self.assertEqual(3. * u.s, test.z)

    def test_redefine_function_binder(self):
        test = SimpleClass(x=Binder(value=1. * u.kg, func1=Function(SimpleClass.func, y_var='y')),
                           y=Binder(unit=u.kg, func1=Function(SimpleClass.func_inv, x_var='x')),
                           z=Binder(value=3. * u.s))
        self.assertEqual(1. * u.kg, test.x)
        self.assertEqual(0.5 * u.kg, test.y)
        self.assertEqual(3. * u.s, test.z)
        test.y = Binder(value=2. * u.kg)
        self.assertEqual(4. * u.kg, test.x)
        self.assertEqual(2. * u.kg, test.y)
        self.assertEqual(3. * u.s, test.z)


@logged
class TestFunction(unittest.TestCase):
    def setUp(self):
        self.func = Function(rho_from_v, v='v')
        self.func_instance = Function(SimpleClass.func, y_var='y')
        self.y = 2

    def test_call(self):
        self.assertEqual(self.func(v=2), 0.5)
        self.assertEqual(self.func_instance(self, y_var=2), 4)

    def test_docstring(self):
        self.assertTrue('Test documentation' in self.func.__doc__)

    def test_repr(self):
        self.assertEqual(r"<func: 'rho_from_v', with kwargs: {'v': 'v'}>", repr(self.func))

    def test_instance_function(self):
        self.assertTrue(self.func_instance.isinstance_function)
        self.assertFalse(self.func.isinstance_function)


@logged
class TestSubscriptionList(unittest.TestCase):
    def setUp(self):
        self.bin1 = Binder(func1=Function(rho_from_gamma, gamma='gamma_attr'))
        self.bin1.key = 'bin1'
        self.bin1.ref_key = 'gamma_attr'
        self.bin2 = Binder(func1=Function(rho_from_v, v='v_attr'))
        self.bin2.key = 'bin2'
        self.bin2.ref_key = 'v_attr'
        self.bin3 = Binder(func1=Function(rho_from_mu_nu, mu='mu_attr', nu='nu_attr'))
        self.bin3.key = 'bin3'
        self.bin3.ref_key = 'nu_attr'
        self.bin4 = Binder()
        self.bin4.key = 'bin4'
        self.bin4.ref_key = 'mu_attr'
        self.subscriptions = SubscriptionList(
                [self.bin1.subscriptions[0], self.bin2.subscriptions[0], self.bin3.subscriptions[0]])

    def test_order(self):
        self.assertListEqual([self.bin1.subscriptions[0], self.bin2.subscriptions[0], self.bin3.subscriptions[0]],
                             self.subscriptions.data)
        self.subscriptions.order(self.bin2)
        self.assertListEqual([self.bin2.subscriptions[0], self.bin1.subscriptions[0], self.bin3.subscriptions[0]],
                             self.subscriptions.data)
        self.subscriptions.order(self.bin1, self.bin3)
        self.assertListEqual([self.bin1.subscriptions[0], self.bin3.subscriptions[0], self.bin2.subscriptions[0]],
                             self.subscriptions.data)
        self.subscriptions.order(self.bin4, self.bin3, self.bin1)
        self.assertListEqual([self.bin1.subscriptions[0], self.bin3.subscriptions[0], self.bin2.subscriptions[0]],
                             self.subscriptions.data)


@logged
class TestBinder(unittest.TestCase):
    def test_bounds(self):
        with self.assertRaises(AttributeError):
            binder = Binder(bounds=(10. * u.kg, 20.))

        with self.assertRaises(ValueError):
            binder = Binder(bounds=(10., 20., 30.))

        with self.assertRaises(DimensionalityError):
            binder = Binder(bounds=(10. * u.kg, 20. * u.m))

        with self.assertRaises(DimensionalityError):
            binder = Binder(bounds=(10., 20.) * u.kg, unit=u.m)

        with self.assertRaises(DimensionalityError):
            binder = Binder(bounds=(10. * u.mg, 20. * u.kg), unit=u.m)

        with self.assertRaises(ValueError):
            binder = Binder(value=10. * u.kg, bounds=(20., 30.) * u.kg)

        with self.assertRaises(ValueError):
            binder = Binder(value=[10., 15.] * u.kg, bounds=(20., 30.) * u.kg)

        binder = Binder(value=10. * u.kg, bounds=(0., 30.) * u.kg)
        self.assertEqual(binder.value, 10. * u.kg)

    def test_value(self):
        with self.assertRaises(DimensionalityError):
            binder = Binder(value=10. * u.m, bounds=(0., 30.) * u.kg)

        binder = Binder(value=10. * u.kg, bounds=(0., 30.) * u.kg)
        with self.assertRaises(DimensionalityError):
            binder.value = 10. * u.m

        binder = Binder(value=10., unit=u.kg)
        self.assertEqual(binder.value, 10. * u.kg)

        binder = Binder(value=10. * u.g, unit=u.kg)
        self.assertEqual(binder.value, 1e-2 * u.kg)

        binder = Binder(value=10., bounds=(0., 10.) * u.kg)
        self.assertEqual(binder.value, 10. * u.kg)

    def test_unit(self):
        binder = Binder(unit='kg')
        self.assertEqual(binder.unit, u.kg)

        with self.assertRaises(UndefinedUnitError):
            binder = Binder(unit='strange')

        binder = Binder(value=10. * u.kg)
        self.assertEqual(binder.unit, u.kg)

        binder = Binder(bounds=(10., 10.) * u.kg)
        self.assertEqual(binder.unit, u.kg)

        binder = Binder(value=10.)
        self.assertEqual(binder.unit, u.dimensionless)

    def test_dunders(self):
        binder_A = Binder()
        binder_A.key = 'key'
        binder_A.ref_key = 'ref_key'
        binder_B = Binder()
        binder_B.key = 'key'
        binder_B.ref_key = 'ref_key'
        self.assertTrue(binder_A == binder_B)
        binder_B.key = 'false key'
        self.assertTrue(binder_A != binder_B)


@logged
class TestMTIobj(unittest.TestCase):
    def setUp(self):
        @logged
        class Matter(MTIobj):
            def __init__(self, **kwargs):
                super(Matter, self).__init__(**kwargs)

            rho = Valid()

            gamma = Valid()

            nu = Valid()

            mu = Valid()

            v = Valid()

            name = ''

        self.matter = Matter(
                rho=Binder(value=1000. * u.kg / u.m ** 3,
                           unit=u.kg / u.m ** 3,
                           func1=Function(rho_from_v, v='v'),
                           func2=Function(rho_from_gamma, gamma='gamma')),
                gamma=Binder(unit=u.N / u.m ** 3,
                             func1=Function(gamma_from_v, v='v'),
                             func2=Function(gamma_from_rho, rho='rho')),
                v=Binder(unit=u.m ** 3 / u.kg,
                         func1=Function(v_from_rho, rho='rho'),
                         func2=Function(v_from_gamma, gamma='gamma')),
                mu=Binder(value=0.001 * u.Pa * u.s,
                          unit=u.Pa * u.s,
                          func1=Function(mu_from_nu_rho, nu='nu', rho='rho')),
                nu=Binder(unit=u.mm ** 2 / u.s,
                          func1=Function(nu_from_mu_rho, mu='mu', rho='rho')),
                additional_simple_var='simple var')

    def test_initialization(self):
        matter = self.matter
        rho = 1000. * u.kg / u.m ** 3
        mu = 0.001 * u.Pa * u.s
        self.assertEqual(gamma_from_rho(rho=rho), matter.gamma)
        self.assertEqual(nu_from_mu_rho(mu=mu, rho=rho), matter.nu)
        self.assertTrue(hasattr(matter, 'additional_simple_var'))

    def test_unknownattribute(self):
        with self.assertRaises(AttributeError):
            self.matter.nu = Binder(unit=u.mm ** 2 / u.s, func1=Function(nu_from_mu_rho, mu='unknown', rho='rho'))
            self.matter.rho = 80. * u.kg / u.m ** 3

    def test_set_value(self):
        matter = self.matter
        rho = 800. * u.kg / u.m ** 3
        mu = 0.001 * u.Pa * u.s
        nu = 1.5 * u.mm ** 2 / u.s
        matter.nu = nu
        self.assertEqual(0.0015 * u.Pa * u.s, matter.mu)
        self.assertEqual(nu, matter.nu)
        # TODO: fix function route priority
        # matter.rho = rho
        # self.assertEqual(0.0012 * u.Pa * u.s, matter.mu)
        # self.assertEqual(nu, round(matter.nu, 3))
        # matter.mu = mu
        # self.assertEqual(mu, matter.mu)
        # self.assertEqual(1.5 * u.mm ** 2 / u.s, matter.nu)
        # self.assertEqual(rho, matter.rho)
        # self.assertEqual(v_from_rho(rho), matter.v)
        # self.assertEqual(gamma_from_rho(rho).to('N/m**3'), matter.gamma)
        # self.assertEqual(nu_from_mu_rho(mu=mu, rho=rho).to('m**2/s'), matter.nu)
        # matter.v = 0.01 * u.m ** 3 / u.kg
        # self.assertEqual(100.0 * u.kg / u.m ** 3, matter.rho)
        # self.assertEqual(1.25 * u.mm ** 2 / u.s, matter.nu)
        # self.assertEqual(0.000125 * u.Pa * u.s, matter.mu)


class TestFunctions_function_switch(unittest.TestCase):
    def setUp(self):
        class TestCls:
            def emb(self, t):
                return 2 * t

        self.test_cls = TestCls()

        def z(x, y):
            return x + y

        def y(x):
            return x

        self.z = z
        self.y = y

    def test_function_switch(self):
        self.assertEqual(2, function_switch(self.z, self.y, x=2))
        self.assertEqual(4, function_switch(self.z, self.y, x=2, y=2))

    def test_wrong_functioncall(self):
        with self.assertRaises(TypeError):
            self.assertEqual(2, function_switch(self.z, self.y, unknown_arg=2))

    def test_embedded_function_switch(self):
        self.assertEqual(4, function_switch(self.z, self.y, self.test_cls.emb, t=2))
        self.assertEqual(2, function_switch(self.z, self.y, self.test_cls.emb, x=2))


class TestFunctions_LUT(unittest.TestCase):
    def setUp(self):
        self.x = np.arange(0, 10)
        self.y = np.sin(self.x)

    def test_LUT_magnitude(self):
        lut_func = LUT(x=self.x, y=self.y)
        self.assertEqual(lut_func(x=1), 0.8414709848078965)
        self.assertEqual(lut_func(x=2.5), 0.5252087174427744)

        with self.assertRaises(ValueError):
            self.assertEqual(lut_func(x=2.5 * u.s), 0.5252087174427744)

    def test_LUT_quantity(self):
        lut_func = LUT(x=self.x * u.s, y=self.y * u.m)
        self.assertEqual(lut_func(x=1 * u.s), 0.8414709848078965 * u.m)
        self.assertEqual(lut_func(x=2.5 * u.s), 0.5252087174427744 * u.m)

        with self.assertRaises(ValueError):
            self.assertEqual(lut_func(x=2.5), 0.5252087174427744)

    def test_LUT_array_magnitude(self):
        lut_func = LUT(x=self.x, y=self.y)
        np.testing.assert_array_equal(lut_func(x=np.array([1, 1.5, 2])),
                                      np.array([0.8414709848078965, 0.8753842058167891, 0.9092974268256817]))

    def test_LUT_unequal_length(self):
        x = self.x[:-2]
        with self.assertRaises(ValueError):
            lut_func = LUT(x=x, y=self.y)

        with self.assertRaises(ValueError):
            lut_func = LUT(x=x * u.s, y=self.y * u.m)

    def test_to_many_arguments(self):
        lut_func = LUT(x=self.x * u.s, y=self.y * u.m)
        with self.assertRaises(TypeError):
            lut_func(x=2 * u.s, to_many=2 * u.s)

        with self.assertRaises(TypeError):
            lut_func = LUT(x=self.x * u.s, y=self.y * u.m, to_many=self.x * u.s)

    def test_repr(self):
        lut_func = LUT(x=self.x * u.s, y=self.y * u.m)
        self.assertEqual(r'<LUT {}>'.format(lut_func.kwargs), repr(lut_func))


class TestCore(unittest.TestCase):
    def test_set_printoptions(self):
        set_printoptions(precision=4)
        value = '{}'.format(1.00156789 * u.m)
        self.assertEqual('1.002 m', value)
        set_printoptions(precision=5)
        value = '{}'.format(1.00156789 * u.m)
        self.assertEqual('1.0016 m', value)

    def test_embedded_attr(self):
        class embedded_tester:
            x = 2.

        class tester:
            y = 1.
            z = embedded_tester()

        test_obj = tester()
        self.assertEqual(1., rgetattr(test_obj, 'y'))
        self.assertEqual(2., rgetattr(test_obj, 'z.x'))
        rsetattr(test_obj, 'y', 100.)
        rsetattr(test_obj, 'z.x', 200.)
        self.assertEqual(100., rgetattr(test_obj, 'y'))
        self.assertEqual(200., rgetattr(test_obj, 'z.x'))


class TestProduct(unittest.TestCase):
    def setUp(self):
        self.p = Product([4., 2.5, 5., 2.])

    def test_assertions(self):
        self.assertTrue(self.p == 100.)
        self.assertFalse(self.p != 100.)
        self.assertTrue(self.p < 200.)
        self.assertTrue(self.p <= 200.)
        self.assertTrue(self.p > 2.)
        self.assertTrue(self.p >= 2.)

    def test_product(self):
        self.assertEqual(100., self.p)
        self.assertEqual(12.5, self.p[1:3])

    def test_add(self):
        self.assertEqual(105., self.p + 5.)
        self.assertEqual(105., 5. + self.p)

    def test_sub(self):
        self.assertEqual(95., self.p - 5.)
        self.assertEqual(-95., 5. - self.p)

    def test_mul(self):
        self.assertEqual(200., self.p * 2.)
        self.assertEqual(200., 2. * self.p)

    def test_truediv(self):
        self.assertEqual(50., self.p / 2.)
        self.assertEqual(2., 200. / self.p)

    def test_pow(self):
        self.assertEqual(10000., self.p ** 2.)
        self.assertEqual(1.2676506002282294e+30, 2. ** self.p)

    def test_mod(self):
        self.assertEqual(30., self.p % 70.)
        self.assertEqual(1., 201. % self.p)

    def test_repr(self):
        self.assertEqual(repr(100.), repr(self.p))

    def test_abs(self):
        self.assertEqual(100., abs(self.p))

    def test_float(self):
        self.assertTrue(isinstance(float(self.p), float))
        self.assertEqual(100., float(self.p))

    def test_int(self):
        self.assertTrue(isinstance(int(self.p), int))
        self.assertEqual(100, int(self.p))

    def test_neg_pos(self):
        self.assertEqual(-100., -self.p)
        self.assertEqual(100., +self.p)

    def test_idx(self):
        arr = range(100, 300, 1)
        self.assertEqual(200, arr[self.p])
