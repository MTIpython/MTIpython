import os
import sys

sys.path.extend([os.path.join(os.path.dirname(__file__), r'../../'),
                 os.path.join(os.path.dirname(__file__), r'../'),
                 os.path.join(os.path.dirname(__file__), r'../MTIpython'),
                 os.path.join(os.path.dirname(__file__), r'.')])

from MTIpython.units import *

import unittest
from dill import dump, load


class TestUnits(unittest.TestCase):
    def test_serialize_unit(self):
        r"""
        Tests the (de)serialize function for Units
        :return: 
        """
        x = 10. * u['m/s']
        with open('TestUnits_x.tst', 'wb') as f:
            dump(x, f)
        with open('TestUnits_x.tst', 'rb') as f:
            y = load(f)
        self.assertEqual(x, y)
        os.remove('TestUnits_x.tst')


if __name__ == '__main__':
    unittest.main()
