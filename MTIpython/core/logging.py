r"""
.. module:: MTIpython.principal.logging
    :platform: Unix, Windows
    :synopsis: Module for logging setup and integration

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>

.. py:data:: Logging Levels

   The numeric values of logging levels are given in the following table. These are primarily of interest if you want
   to define your own levels, and need them to have specific values relative to the predefined levels. If you define
   a level with the same numeric value, it overwrites the predefined value; the predefined name is lost.

   .. list-table:: Logging Levels
       :widths: 50 50
       :header-rows: 1

       * - Level
         - Numeric value
       * - CRITICAL
         - 50
       * - ERROR
         - 40
       * - WARNING
         - 30
       * - INFO
         - 20
       * - DEBUG
         - 10
       * - NOTSET
         - 0

"""
import logging
import logging.config
import os
from logging import CRITICAL, DEBUG, ERROR, INFO, NOTSET, WARNING, getLogger

import yaml
from pkg_resources import resource_filename

std_path = resource_filename(__name__, '../resources/logging/logging.yaml')

__all__ = ['setup_logging', 'logged', 'INFO', 'WARNING', 'ERROR', 'DEBUG', 'CRITICAL', 'NOTSET', 'getLogger']


def setup_logging(default_path: str = None, default_level: int = logging.INFO, env_key: str = 'LOG_CFG'):
    r"""
    Setups a logging configuration

    Args:
        default_path (str): Path to the yaml file containing the configuration
        default_level (int): default logging level of configuration when logging configuration couldn't be found
        env_key (str): The ENVIRONMENT VARIABLE with the path tot the logging file
    """
    if default_path is not None:
        path = default_path
    else:
        path = std_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.getLogger(__name__).setLevel(default_level)
        raise OSError('Logging configuration file {} not found, setting up basic config'.format(default_path))
    env = "{" + "\n".join("{}: {}".format(k, v) for k, v in os.environ.items()) + "}"
    header = \
        '\n\n****************************************************************************************************\n' \
        '*                                         MTIpython log                                            *\n' \
        '****************************************************************************************************\n' \
        'Default log level: {} \n' \
        'Current environment:\n {} \n' \
        '****************************************************************************************************\n' \
        '*                                                                                                  *\n' \
        '****************************************************************************************************\n' \
        '\n'.format(
                logging.getLogger(__name__).getEffectiveLevel(), env)
    logging.getLogger(__name__).log(100, header)
    logging.getLogger('pint.util').setLevel(ERROR)


def logged(obj):
    r"""
    Decorator to make sure that all loggers use the same setup

    Args:
        cls (object): Object to be logged

    Returns:
        (object): an object decorated with a logger attribute
    """
    if isinstance(obj, object):
        obj.logger = logging.getLogger('{}.{}'.format(obj.__module__, obj.__qualname__))
        obj.logger.__doc__ = r"The object logger"
    return obj
