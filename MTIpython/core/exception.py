r"""
.. module:: MTIpython.principal.exceptions
    :platform: Unix, Windows
    :synopsis: Module with exceptions warnings

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

from warnings import warn

from pint.errors import DimensionalityError, UndefinedUnitError

__all__ = ['LoadException', 'DimensionalityError', 'UndefinedUnitError', 'BinderValueWarning', 'warn']


class LoadException(Exception):
    r"""
    Raised when the stored object is incompatible with the object it is to be serialized in
    """
    pass


class BinderValueWarning(UserWarning):
    r"""
    Raised when a :class:`MTIpython.principal.binder.Binder` object is unable to calculate a new value using the given
    bindings
    """
    pass
