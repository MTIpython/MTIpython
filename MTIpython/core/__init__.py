r"""
.. module:: MTIpython.principal
    :platform: Unix, Windows
    :synopsis: packages with universal MTI objects and functions

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

from MTIpython.core import binder, core, exception, functions, logging, mtiobj

__all__ = ['core', 'logging', 'exception', 'binder', 'mtiobj', 'functions']
