import inspect
from collections import UserList

from numpy import array, sort

from MTIpython.core.exception import BinderValueWarning, DimensionalityError, UndefinedUnitError, \
    warn
from MTIpython.core.logging import logged
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import isiterable, isquantity, isunit, u

__all__ = ['Binder', 'Valid', 'Function', 'SubscriptionList', 'lock', 'unlock']


@logged
class Function:
    def __init__(self, function, **kwargs):
        r"""
        A function binding class used in conjunction :class:`.Binder`, where this function allows
        for the binding of
        :class:`.MTIobj` attributes.

        Example usage with a regular callable object:

            >>> def f(var1, var2):
            >>>     return var1 * var2

            >>> func = Function(f, var1='attr1', var2='attr2')

            >>> func(var1=1, var2=2)
            2

        Example usage with :class:`.LUT` object

            >>> lut_func = LUT(x=[1., 2., 3.] * u.s, y=[0.5, 1., 1.5] * u.m)
            >>> func = Function(lut_func, x='atrr1')
            >>> func(1.5 * u.s)
            <Quantity(0.75, 'meter')>

        Args:
            function (callable): a function pointer
            source (str): Containing the bibtex citation from where the function is referenced
            **kwargs: The keyword arguments of the function, where the values are (str) with
            attributes names.
        """
        self.function = function
        if hasattr(self.function, '__name__'):
            self.__name__ = self.function.__name__

        # determine if function is a instance function
        func_kw = inspect.getfullargspec(function).args
        if len(func_kw) > 0:
            if func_kw[0] == 'self':
                if self.__name__ == 'LUT':
                    self.isinstance_function = False
                else:
                    self.isinstance_function = True
                del func_kw[0]

        r""" TODO: Check the sanity of the function against the specified keywords, keep in mind 
        that a function can 
        also have var kwargs"""
        self.kwargs = kwargs
        self.attributes = list(self.kwargs.values())

        # Remove the self attribute from the attributes list
        if self.isinstance_function:
            del self.attributes[0]

        self.match = 0
        self.__doc__ = self.function.__doc__

    def __call__(self, *args, **kwargs):
        return self.function(*args, **kwargs)

    def __repr__(self):
        try:
            return '<func: {}, with kwargs: {}>'.format(repr(self.function.__name__),
                                                        repr(self.kwargs))
        except:
            return "Unable to get repr for <class '{}'>".format(type(self))

    __doc__ = ''

    __name__ = ''

    function = None
    r"""function: Pointer to the stored function"""

    kwargs = {}
    r"""dict(str, str): The keyword arguments of the function, where the values are (str) with 
    attributes names"""

    isinstance_function = False
    r"""bool: Indicates if a function belongs to a class and therefor it should be passed the 
    instance"""

    attributes = []
    r"""list(str): A list of attributes"""

    match = 0
    r"""int: Indication how well the function matches the requested arguments, used in 
    conjunction with 
    :class:`~SubscriptionList`"""


@logged
class SubscriptionList(UserList):
    r"""
    A list storing :class:`.Functions` which can be ordered using :class:`.Binders`,
    where user-set :class:`.Binders`
    account for two, and have a higher ranking.

    Example usage:

        >>> def func_1(x, y):
        >>>     return x + y
        >>> def func_2(x):
        >>>     return x
        >>> def func_3(z)
        >>>     return z
        >>>
        >>> class Simple(MTIobj):
        >>>     x_attr = Valid()
        >>>     y_attr = Valid()
        >>>     z_attr = Valid()
        >>>
        >>> simple = Simple(x=Binder())
        >>> ss = SubscriptionList([Function(func_1, x='x_attr', y='y_attr'),
        >>>                        Function(func_2, x='x_attr'),
        >>>                        Function(func_3, z='z_attr')])
        >>> ss
        [<func: 'func_2', with kwargs: {'x': 'x_attr', 'y': 'y_attr'}>, <func: 'func_1',
        with kwargs: {'x': 'x_attr'}>,
        <func: 'func_3', with kwargs: {'z': 'z_attr'}>]
        >>> ss.order(simple.x_attr)
        [<func: 'func_1', with kwargs: {'x': 'x_attr'}>, <func: 'func_2', with kwargs: {'x':
        'x_attr', 'y': 'y_attr'}>,
        <func: 'func_3', with kwargs: {'z': 'z_attr'}>]
        >>> ss.order(simple.x_attr, simple.y_attr)
        [<func: 'func_2', with kwargs: {'x': 'x_attr', 'y': 'y_attr'}>, <func: 'func_1',
        with kwargs: {'x': 'x_attr'}>,
        <func: 'func_3', with kwargs: {'z': 'z_attr'}>]
        >>> ss.order(simple.z_attr)
        [<func: 'func_3', with kwargs: {'z': 'z_attr'}>, <func: 'func_2', with kwargs: {'x':
        'x_attr', 'y': 'y_attr'}>,
         <func: 'func_1', with kwargs: {'x': 'x_attr'}>]
    """

    def order(self, *args):
        r"""
        Orders a SubscriptionList, with respect to the given :class:`.Binders`.

        Args:
            *args (:class:`.Binders`): Positional argument Binders
        """

        # Reset the attribute function match counter
        for func in self.data:
            func.match = 0

        # Add 1 when a changed arg is also needed by the function
        userset = 1
        for arg in args:
            for func in self.data:
                if arg.ref_key in func.attributes:
                    if arg.userset:
                        userset += 1
                    func.match += 1

        # Normalize the match using the total number of function arguments
        for func in self.data:
            func.match /= len(func.attributes) if len(func.attributes) > 0 else 1
            func.match *= userset

        # sort the list
        self.data.sort(key=lambda x: x.match, reverse=True)


@logged
class Binder:
    def __init__(self, value=None, bounds=(-inf, inf), unit=None, **kwargs):
        r"""
        A Binder class, which sets up a subscription, subscriber network.

        Common usage:

            >>> def v_from_rho(rho_var):
            >>>     return rho ** -1

            >>> def rho_from_v(v_var):
            >>>     rerturn var ** -1

            >>> class Test(MTIobj):
            >>>     def __init__(self, **kwargs):
            >>>         super(Test, self).__init__(**kwargs)
            >>>     v = Valid()
            >>>     rho = Valid()

            >>> test = Test(rho=Binder(value=1000. * u.kg / u.m ** 3, bounds=(0., inf),
            >>>                        func1=Function(rho_from_v, v_var='v')),
            >>>             v=Binder(unit=u.m ** 3 / u.kg, bounds=(0., 1.),
            >>>                      func1=Function(v_from_rho, rho_var='rho')))
            >>> test.gamma
            <Quantity(0.001, 'meter ** 3 / kilogram')>
            >>> test.gamma = 0.01 * u.m ** 3 / u.kg
            test.rho
            <Quantity(100.0, 'kilogram / meter ** 3')>

        Args:
            value (:class:`~pint:.Quantity` or :class:`.Binder`): A initial value
            bounds tuple(:class:`~pint:.Quantity`, :class:`~pint:.Quantity`): the bounds wherein
             the value must lay, default -inf ... inf
            unit (:class:`~pint:.Unit`): Unit in which the value should be returned, if a value
             isn't specified, a unit must be set, otherwise optional value, because the unit will
             be determined from the units used by value or bounds
            **kwargs: :class:`.Function` objects that describe the relation of other class
             :class:`.Valid` attributes to this Attribute. keywords are set to be func1, func2,
             func3 ... func4 etc.
        """

        # If the value to be passed is another Binder object, copy __dict__ and by pass other checks
        if isinstance(value, Binder):
            for attr in value.__dict__.keys():
                setattr(self, attr, getattr(value, attr))
            return

        # Check the dimensions against each other
        values = [value, bounds, unit]
        quantities_used = [v.u if isquantity(v) else v if isunit(v) else None for v in values]
        units_used = [q for q in quantities_used if q is not None]
        dims_used = [u.get_dimensionality(uu) for uu in units_used if uu is not None]
        dims = set(dims_used)
        if len(dims) > 1:
            raise DimensionalityError(units1=units_used[0],
                                      units2=units_used[1])

        # Determine and set the unit
        if len(set(units_used)) == 1:
            self.unit = units_used[0]
        elif unit is not None:
            if isunit(unit):
                self.unit = unit
            elif isinstance(unit, str):
                try:
                    self.unit = u[unit].u
                except UndefinedUnitError as e:
                    e.args = e.args + ('Unit is not defined in the unit registry',)
                    raise
        elif value is not None and isquantity(value):
            self.unit = value.u
        elif isquantity(bounds):
            self.unit = bounds.u
        else:
            self.unit = u.dimensionless

        # Set the bounds and make sure that the bounds are <Quantity([lower, upper]), unit>
        self.bounds = bounds

        # Set the value
        self.value = value
        self.userset = value is not None

        # Set the subscriptions
        self.subscriptions = SubscriptionList([kwargs.get(f) for f in kwargs.keys() if 'func' in f])

        # Set the subscribers as an empty set. To be filled at an latter stage
        self.subscribers = set()

    def __repr__(self):
        try:
            return '<Key: {}, Value: {}, Bound: {}>'.format(self.ref_key, repr(self._value),
                                                            repr(self._bounds))
        except:
            return "Unable to get repr for <class '{}'>".format(type(self))

    def __eq__(self, other):
        return (self.ref_key, self.key) == (other.ref_key, other.key)

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash((self.ref_key, self.key))

    key = ''
    r"""str: The key for which this Binder is set"""

    ref_key = ''
    r"""str: The reference key for which this Binder is set"""

    unit = None
    r""":class:`~pint:.Unit`: The unit in which the value should be returned"""

    subscriptions = SubscriptionList()
    r""":class:`.Function`: All the subscriptions, from which the Binder can obtain its value"""

    subscribers = set()
    r"""set(str): All the other binders, whom subscribe to this Binder"""

    userset = False
    r"""bool: Is the value set by a user"""

    lock = False
    r"""bool: Locks the value from updating"""

    @property
    def value(self):
        r"""Quantity: The current value"""
        if isiterable(self._value) and all(self._value) or self._value is not None:
            return self._value * self.unit
        else:
            return None

    @value.setter
    def value(self, value):
        if value is not None:
            if isquantity(value):
                try:
                    value = value.to(self.unit).m
                except DimensionalityError as e:
                    e.extra_msg = '\n Setting {} with a value of {} raise: \n {}'.format(
                            repr(self),
                            repr(value),
                            e.extra_msg)
                    raise
            if self.iswithin_bound(value):
                self._value = value
            else:
                raise ValueError(
                        'Value {} out of bounds {} for {}!'.format(repr(value), repr(self.bounds),
                                                                   self.key[8:]))
        else:
            self._value = None

    @property
    def bounds(self):
        r"""tuple(:class:`~pint:.Quantity`, :class:`~pint:.Quantity`): The bounds of the value"""
        return self._bounds * self.unit

    @bounds.setter
    def bounds(self, value):
        # Make sure that the bounds are <Quantity([lower, upper]), unit>
        if not isquantity(value) and isquantity(value[0]):
            try:
                value = [b.to(value[0].u).m for b in value] * value[0].u
            except AttributeError as e:
                e.args = e.args + ('Not all bound values in bounds are Quantities!',)
                raise
            except DimensionalityError as e:
                e.args = e.args + ('Bound values in bounds have different dimensions!',)
                raise
        if isquantity(value):
            try:
                value = value.to(self.unit).m
            except DimensionalityError as e:
                e.args = e.args + ('Dimensionality of bounds differs from Check.unit',)
                raise

        # Convert the bounds to numpy.ndarray
        value = array(value)
        # Flatten the array
        value = value.flatten()
        # Check is
        if len(value) != 2:
            raise ValueError('Bounds should have a length of 2')
        # Make sure the values are lower -> upper
        value = sort(value)
        # Set the hidden bounds
        self._bounds = value

    def iswithin_bound(self, value):
        r"""
        Checks if the value is within the bounds

        Args:
            value (:class:`~pint:.Quantity`): The value to be checked

        Returns:
            bool: True if within bounds
        """
        if isiterable(value):
            return all(self._bounds[0] <= value) and all(value <= self._bounds[-1])
        else:
            return self._bounds[0] <= value <= self._bounds[-1]

    def update(self, instance, userset=False, update_affected=True):
        r"""
        Update the Binder value, by iterating over its subscriptions

        Args:
            instance (:class:`.MTIobj`): The instance to which the Binder and its subscriptions
             belong.
            userset (bool): Is the value set by the user (default) False
            update_affected (bool): Should the subscribers be updated when this value is changed

        Raises:
            AttributeError: When requested attribute doesn't exist in the instance
        """

        # set iteration logic
        if not instance._user_set_valid:
            instance._user_set_valid = self.key
            instance._updating = True

        # If attribute is all ready visited, don't perform calc again
        if self in instance._visited_valids or not update_affected:
            return

        # Add attribute to visited nodes
        instance._visited_valids.add(self)

        # Order the subscription list, giving newly updated variable preference over unknown
        # variable
        self.subscriptions.order(*instance._visited_attr)

        update_affected = True if userset else False
        if not userset:
            # Obtain the value, by looping through subscription functions
            value_obtained = False
            for func in self.subscriptions:
                # get the requested values for each function argument
                kwargs = {}
                for arg, attr in func.kwargs.items():
                    try:
                        kwargs[arg] = getattr(instance, attr)  # TODO: use custom embedded getter
                    except AttributeError as e:
                        e.args = e.args + (
                            'Attribute: {} not known to instance: {}'.format(arg, instance),)
                        raise
                try:
                    self.logger.debug(
                            'Updating: {} using function {} with the following args {}'.format(
                                    self.ref_key,
                                    func.__name__,
                                    kwargs))
                    if func.isinstance_function:  # If the function is an instance function,
                        # pass the instance along
                        value = func(instance, **kwargs)
                    else:  # If function is of static type, pass only arguments along
                        value = func(**kwargs)
                except:  # If function failed, presumably because one of the arguments was a None
                    #  value, set value to
                    #  None
                    value = None
                # If value is differs from previous, set the value of the Binder object to the
                # obtained value
                if value is not None:  # and self.value != value:
                    self.value = value
                    self.logger.debug('New value is {}'.format(self.value))
                    update_affected = True
                    self.userset = userset
                    value_obtained = True
                    break
            if not value_obtained:
                warn('Could not obtain a value from subscriptions for Valid {}!'.format(
                        self.ref_key), BinderValueWarning)

        # Value is changed and can be used by other Valids
        if self not in instance._visited_attr:
            instance._visited_attr.append(self)

        # Update affected subscribers
        for subscriber in instance.__dict__[self.key].subscribers:
            instance.__dict__[subscriber].update(instance=instance, update_affected=update_affected)

        # If the first requesting attribute has obtained his value, reset iteration logic
        if instance._user_set_valid == self.key:
            instance._user_set_valid = None
            instance._visited_valids = set()
            instance._updating = False
            instance._visited_attr = []


@logged
class Valid:
    def __init__(self):
        r"""
        An attribute descriptor, guarding the user input, checking against units and bounds. This
        descriptor works closely with :class:`.Binder`
        """
        pass

    key = ''
    r"""
    str: The key of the hidden attribute, where the :class:`~Valid` is stored in the format 
    '_Valid__<attribute name>'. 
    This key is set by the metaclass :class:`.MeTaIpython` 
    """

    ref_key = ''
    r"""
    str: The reference key from where the value getter/setter is accessed. This key is set by the 
    metaclass
    :class:`.MeTaIpython`
    """

    def __get__(self, instance, owner):
        value = instance.__dict__[self.key].value
        if value is not None:
            value._binder_ref = self.key
            value._instance = instance
        return value

    def __set__(self, instance, value):
        if isinstance(value, Binder):  # A new Binder object is to be set in the Valid descriptor
            value.key = self.key
            value.ref_key = self.ref_key
            instance.__dict__[self.key] = value
            # Reset all subscriber
            if not instance._initialization:
                instance.set_all_subscribers()
                instance.__dict__[self.key].update(instance, userset=True, update_affected=True)
            self.logger.debug(
                    'Setting a new Binder {}, for instance {}'.format(repr(value), repr(instance)))
        else:  # A new value is to be passed along the Binder object associated  with the Valid
            # descriptor
            isiter_instance = isiterable(instance.__dict__[self.key].value)
            isiter_value = isiterable(value)
            if isiter_instance == isiter_value:
                if isiter_instance:
                    if any(instance.__dict__[self.key].value != value):
                        self._update_instance(instance, value)
                else:
                    if instance.__dict__[self.key].value != value:
                        self._update_instance(instance, value)
            else:
                self._update_instance(instance, value)

    def _update_instance(self, instance, value):
        if not instance.__dict__[self.key].lock:
            instance.__dict__[self.key].value = value
            self.logger.debug(
                    'Setting a new value {}, to Binder {}, for instance {}'.format(repr(value),
                                                                                   self.key,
                                                                                   repr(instance)))
            instance.__dict__[self.key].update(instance, userset=True, update_affected=True)


def lock(valid):
    r"""
    Prevents the Valid from being updated

    Args:
        valid: Valid attribute

    Returns:
        None
    """
    valid._instance.__dict__[valid._binder_ref].lock = True


def unlock(valid):
    r"""
    Enables a locked Valid to update

    Args:
        valid: Valid attribute

    Returns:
        None
    """
    valid._instance.__dict__[valid._binder_ref].lock = False
