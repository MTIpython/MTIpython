r"""
.. module:: MTIpython.principal.mtiobj
    :platform: Unix, Windows
    :synopsis: MTIobj module containing the base metaclass for all MTIpython types and base class type for all
     MTIpython classes

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from functools import reduce

from numpy import product, set_printoptions as np_set_printoptions

from MTIpython.units.SI import u

__all__ = ['Singleton', 'rsetattr', 'rgetattr', 'rhasattr', 'set_printoptions', 'Product']


def set_printoptions(precision=3):
    r"""
    sets the MTIpython print options

    Args:
        precision: Number of digits of precision for floating point output (default 3)
    """
    np_set_printoptions(precision=precision)
    u.default_format = '{}.{}'.format(u.default_format[:1], precision)


class Singleton(type):
    r"""
    Singleton base class. Inherit from this type when only one instance is allowed to run from its child class
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


sentinel = object()
r"""
Sentinel guarding object used by :func:`rgetattr`
"""


def rsetattr(obj, attr, val):
    r"""
    Embedded Attribute setter.

    Example usage:

        >>> class EmbeddedClass:
        >>>     z = 2
        >>>
        >>> class MainClass:
        >>>     x = EmbeddedClass()
        >>>     y = 1
        >>>
        >>> example_obj = MainClass()
        >>> example_obj.y
        1
        >>> rsetattr(example_obj, 'y', 2)
        >>> example_obj.y
        2
        >>> example_obj.x.z
        2
        >>> rsetattr(example_obj, 'x.z', 3)
        >>> example_obj.x.z
        3

    Args:
        obj (type): The object in which the attribute resides
        attr (str): The attribute which to set, use dot notation to access embedded attributes, see example above
        val (type): The value with which to set the attribute

    """
    pre, _, post = attr.rpartition('.')
    return setattr(rgetattr(obj, pre) if pre else obj, post, val)


def rgetattr(obj, attr, default=sentinel):
    r"""
    Embedded Attribute getter.

    Example usage:

        >>> class EmbeddedClass:
        >>>     z = 2
        >>>
        >>> class MainClass:
        >>>     x = EmbeddedClass()
        >>>     y = 1
        >>>
        >>> example_obj = MainClass()
        >>> rgetattr(example_obj, 'y')
        1
        >>> rgetattr(example_obj, 'x.z')
        2

    Args:
        obj (type): The object in which the attribute resides
        attr (str): The attribute which to set, use dot notation to access embedded attributes, see example above
        default (object): default object

    Returns:
        object: to be returned
    """
    if default is sentinel:
        _getattr = getattr
    else:
        def _getattr(obj, name):
            return getattr(obj, name, default)
    return reduce(_getattr, [obj] + attr.split('.'))


def rhasattr(obj, attr):
    r"""
    Embedded hasattr.

    Example usage:

        >>> class EmbeddedClass:
        >>>     z = 2
        >>>
        >>> class MainClass:
        >>>     x = EmbeddedClass()
        >>>     y = 1
        >>>
        >>> example_obj = MainClass()
        >>> rhasattr(example_obj, 'y')
        True
        >>> rhasattr(example_obj, 'x.z')
        True
        >>> rhasattr(example_obj, 'x.unknown')
        False


    Args:
        obj (type): The object in which the attribute resides
        attr (str): The attribute which to set, use dot notation to access embedded attributes, see example above

    Returns:
        bool: Return whether the embedded object has an attribute with the given name
    """
    pre, _, post = attr.rpartition('.')
    return hasattr(rgetattr(obj, pre) if pre else obj, post)


class Product:
    def __init__(self, data):
        r"""
        The product of a list.

        Example usage:

            >>> product = Product([1.5, 3., 6., 4.5, 6.])
            >>> product
            729.0
            >>> product / 3
            243.0
            >>> product[1:3]
            18.0
            >>> product[1:3] * 3
            2187.0

        Args:
            data (:class:`~python:list` or :class:`~numpy:numpy.ndarray`): initialization list
        """
        self.data = data

    def __getitem__(self, item):
        return product(self.data[item])

    def __repr__(self):
        return repr(product(self.data))

    def __add__(self, other):
        return product(self.data) + other

    def __radd__(self, other):
        return product(self.data) + other

    def __sub__(self, other):
        return product(self.data) - other

    def __rsub__(self, other):
        return other - product(self.data)

    def __truediv__(self, other):
        return product(self.data) / other

    def __rtruediv__(self, other):
        return other / product(self.data)

    def __mul__(self, other):
        return product(self.data) * other

    def __rmul__(self, other):
        return self.__mul__(other)

    def __pow__(self, power, modulo=None):
        return pow(product(self.data), power)

    def __rpow__(self, other, modulo=None):
        return pow(other, product(self.data))

    def __mod__(self, other):
        return product(self.data) % other

    def __rmod__(self, other):
        return other % product(self.data)

    def __lt__(self, other):
        return product(self.data) < other

    def __le__(self, other):
        return product(self.data) <= other

    def __eq__(self, other):
        return product(self.data) == other

    def __ne__(self, other):
        return product(self.data) != other

    def __ge__(self, other):
        return product(self.data) >= other

    def __gt__(self, other):
        return product(self.data) > other

    def __abs__(self):
        return abs(product(self.data))

    def __float__(self):
        return float(product(self.data))

    def __int__(self):
        return int(product(self.data))

    def __index__(self):
        return int(self)

    def __neg__(self):
        return -product(self.data)

    def __pos__(self):
        return +product(self.data)
