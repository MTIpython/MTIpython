r"""
.. module:: MTIpython.principal.functions
    :platform: Unix, Windows
    :synopsis: Module with helper functions.

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

import inspect

from numpy import interp

from MTIpython.core.mtiobj import MTIobj
from MTIpython.units.SI import arequantities, isquantity

__all__ = ['LUT', 'function_switch', 'MTIfunc']


class LUT(MTIobj):
    def __init__(self, **kwargs):
        r"""
        Create a Look-Up-Table function

        Example:
            >>> lut_func = LUT(x=[1., 2., 3.] * u.s, y=[0.5, 1., 1.5] * u.m)
            >>> lut_func(1.5 * u.s)
            <Quantity(0.75, 'meter')>

        Args:
            **kwargs: The variable, where the first specified variable is along the x-axis and the second along the
             y-axis

        Raise:
            ValueError: When len(x) != len(y)
        """

        if len(kwargs) != 2:
            raise TypeError('Should pass two argument to function!')
        args = tuple(kwargs.values())
        kw = tuple(kwargs.keys())
        if arequantities(*args):
            if not len(args[0].m) == len(args[1].m) or len(args[0].m) < 2:
                raise ValueError('{} != {} and/or length < 2!'.format(kw[0], kw[1]))
            self._quantities = True
        else:
            if not len(args[0]) == len(args[1]) or len(args[0]) < 2:
                raise ValueError('x != y and/or length < 2!')
            self._quantities = False

        self.kwargs = kwargs
        self._ret_kw = kw[1]
        super(LUT, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        if len(kwargs) != 1:
            raise TypeError('Should pass one argument to function!')
        arg = list(kwargs.values())[0]
        kw = list(kwargs.keys())[0]
        if isquantity(arg):
            if self._quantities:
                return interp(arg.to(self.kwargs[kw].u).m, self.kwargs[kw].m, self.kwargs[self._ret_kw].m) * \
                       self.kwargs[self._ret_kw].u
            else:
                raise ValueError('Trying to pass a Quantity to a none Quantity LUT class')
        else:
            if self._quantities:
                raise ValueError('Trying to pass a magnitude to a Quantity LUT class')
            else:
                return interp(arg, self.kwargs[kw], self.kwargs[self._ret_kw])

    def __repr__(self):
        try:
            return '<LUT {}>'.format(self.kwargs)
        except:
            return "Unable to get repr for <class '{}'>".format(type(self))

    __name__ = 'LUT'

    kwargs = {}
    r""" dict(str, :class:`~numpy:numpy.ndarray`): Keyword with look-up-array"""


def function_switch(*args, **kwargs):
    r"""
    A function switch, allowing the keyword arguments to determine which function should be called.
    
    Example usage:
        >>> def z(x, y):
        >>>     return x + y
        >>>
        >>> def y(x):
        >>>     return x
        >>>
        >>> function_switch(z, y, x=2)
        2
        >>> function_switch(z, y, x=2, y=2)
        4

    Args:
        *args: The functions from which to choose
        **kwargs: The keywords that determine which function should return a value

    Returns:
        The return value of the chosen function

    Raises:
        TypeError: When there wasn't a matching key argument found in the switch functions
    """
    for arg in args:
        param_key = inspect.signature(arg).parameters.keys()
        if set(kwargs.keys()) == set(param_key):
            return arg(**kwargs)
    raise TypeError('No fitting function found for the following arguments: {}'.format(kwargs))


class MeTaIfunction(type):
    def __init__(cls, name, bases, attr_dict):
        r"""
        A Metaclass for function classes setting the docstring of the class, to the __call__

        Args:
            name (str): Name of the object
            bases (object): object type
            attr_dict (dict): Attribute dictionary
        """
        super().__init__(name, bases, attr_dict)
        if '__call__' in attr_dict.keys() and '__doc__' in attr_dict.keys():
            attr_dict['__call__'].__doc__ = attr_dict['__doc__']


class MTIfunc(metaclass=MeTaIfunction):
    r"""
    MTI function base class, use this class for all complex function class.
    """

    def __init__(self, *args, **kwargs):
        for k in kwargs.keys():
            setattr(self, k, kwargs[k])
