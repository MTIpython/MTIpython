r"""
.. module:: MTIpython.principal.mtiobj
    :platform: Unix, Windows
    :synopsis: MTIobj module containing the base metaclass for all MTIpython types and base class type for all MTIpython
     classes

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

from dill import dump, load

from MTIpython.core.binder import Binder, Valid
from MTIpython.core.logging import logged
from MTIpython.core.exception import LoadException

__all__ = ['MeTaIpython', 'MTIobj']


class MeTaIpython(type):
    def __init__(cls, name, bases, attr_dict):
        r"""
        A Metaclass for objects which make use of the :class:`.Valid` descriptor

        Args:
            name (str): Name of the object
            bases (object): object type
            attr_dict (dict): Attribute dictionary
        """
        super().__init__(name, bases, attr_dict)
        for key, attr in attr_dict.items():
            if isinstance(attr, Valid):
                type_name = type(attr).__name__
                attr.key = '_{}__{}'.format(type_name, key)
                attr.ref_key = key


@logged
class MTIobj(metaclass=MeTaIpython):
    def __init__(self, **kwargs):
        r"""
        The basis for most MTIpython classes. This class ensures that objects with :class:`.Valid`
        descriptors are setup correctly

        Args:
            **kwargs: Attributes that will be added to the __dict__ dictionary. This allows a user, create his own child
             classes with custom simple attributes or :class:`.Valid` attributes.
        """
        for k in kwargs.keys():
            setattr(self, k, kwargs[k])

        self._visited_valids = set()
        self._visited_attr = []
        self._user_set_valid = None
        self._updating = False
        self.set_all_subscribers()
        self.update_all_subscribers()
        self._initialization = False

    _initialization = True

    _visited_valids = set()

    _visited_attr = []

    _user_set_valid = None

    _updating = False

    _version = 4
    """int: version of the MTIobj class. Bump this value up for big changes in the class which aren't compatible with 
    earlier release. """

    def set_all_subscribers(self):
        r"""
        Loops through all :class:`.Valid` attributes and determines which attributes are dependant
        on each other, adding those to the subscriber list.
        """

        # Clean subscriber
        if not self._initialization:
            for binder in [b for b in self.__dict__.values() if isinstance(b, Binder)]:
                binder.subscribers = set()

        # set all subscriber
        for binder in [b for b in self.__dict__.values() if isinstance(b, Binder)]:
            for func in binder.subscriptions:
                for arg, attr in func.kwargs.items():
                    if hasattr(self, '_Valid__' + attr):
                        sub = self.__dict__['_Valid__' + attr]
                        sub.subscribers.add(binder.key)

    def update_all_subscribers(self):
        r"""
        Update all subscriber :class:`.Valid` values for the object.
        """
        for binder in [b for b in self.__dict__.values() if isinstance(b, Binder)]:
            binder.update(instance=self, userset=binder.value is not None)
        self._visited_attr = []

    def save(self, filename):
        r"""
        Saves a MTIobject to file

        Args:
            filename (str): location of the file

        Note:
            Each saves triggers an info log entry
        """
        with open(filename, 'wb') as f:
            dump(self, f)
        self.logger.info(
            '{} saved to file {} with the following specs: \n{}'.format(self.__class__, filename, repr(self)))

    def load(self, filename):
        r"""
        Loads a previous saved MTIobj. Into the current object.

        Args:
            filename (str): location of the file

        Raises:
            LoadException: When stored object has a different version then the current class

        Note:
            Loading a file triggers an info log entry, specifying the loaded material specifications

        """
        with open(filename, 'rb') as f:
            try:
                mti = load(f)
            except ModuleNotFoundError:
                raise LoadException(
                    'Stored class incompatible with current class version {}'.format(self.__version))
        if mti._version == self._version:
            for attr in mti.__dict__.keys():
                setattr(self, attr, getattr(mti, attr))
            self.logger.info('{} loaded from file {} with the following specs: \n{}'.format(self.__class__, filename, repr(self)))
        else:
            raise LoadException(
                'Stored class version {} incompatible with current class version {}'.format(mti.__version,
                                                                                            self.__version))
