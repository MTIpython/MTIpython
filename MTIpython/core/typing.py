r"""
.. module:: MTIpython.principal.typing
    :platform: Unix, Windows
    :synopsis: Typing helper module

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from fractions import Fraction
from numbers import Number
from typing import *

from numpy.core.multiarray import ndarray
from pint.quantity import _Quantity
from pint.unit import _Unit
from uncertainties.core import UFloat

from MTIpython.core.binder import Binder

__all__ = ['t_amount', 't_unit', 't_binder', 't_liquid', 't_solid', 't_granular']

t_amount = TypeVar('t_amount', Number, UFloat, Fraction, _Quantity, ndarray)
r"""A certain amount of substance, defined as :class:`~pint:pint.UnitRegistry.Quantity`, 
:class:`~python:numbers.Number`, class:`~python:fractions.Fraction`, 
:class:`~uncertainties:uncertainties.principal.UFloat` 
or :class:`~numpy:numpy.ndarray`"""

t_unit = _Unit

t_binder = Union[t_amount, Binder, None]
r"""A certain amount of substance, defined as :class:`~pint:pint.UnitRegistry.Quantity`, 
:class:`~python:numbers.Number`, :class:`~python:fractions.Fraction`, 
:class:`~uncertainties:uncertainties.principal.UFloat` 
, :class:`~numpy:numpy.ndarray` or a :class:`.Binder`"""

t_liquid = Union['MTIpython.material.liquid.Liquid', 'MTIpython.material.liquid.Bingham', 'MTIpython.material.gas.Gas']
r"""A liquid type of material, such as Gas, Liquid and Bingham plastics
"""

t_solid = Union['MTIpython.material.solid.Solid', 'MTIpython.material.metal.Metal', 'MTIpython.material.plastic.Plastic']
r"""A solid type of material, such as Solid, Metal, Plastic
"""

t_granular = Union['MTIpython.material.granular.Granular', 'MTIpython.material.granular.Powder']
r"""A granular material type, such as Granular or Powder"""
