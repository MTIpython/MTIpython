r"""
.. module:: MTIpython
    :platform: Unix, Windows
    :synopsis: MTIpython a collection of python packages for different engineering disciplines

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com> & Maarten in 't Veld <jm.intveld@ihcmti.com>
"""

from MTIpython import core, fluid, material, mech, mtimath, mtiprint, units
from MTIpython.units.SI import u

__all__ = ['core', 'units', 'material', 'fluid', 'mech', 'mtimath', 'mtiprint', 'u']
__version__ = '0.2.dev'
