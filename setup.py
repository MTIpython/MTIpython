import site
import subprocess

from setuptools import find_packages, setup
from setuptools.command.install import install
from setuptools.command.develop import develop


class CustomInstallCommand(install):

    def run(self):
        super(CustomInstallCommand, self).run()
        print('Patching Pint')
        pint_loc = site.getsitepackages()[0]
        subprocess.call(['patch', '-s' , pint_loc + r'/pint/quantity.py',
                         r'patches/Fixed_unsupported_format_string_passed_to_numpy_ndarray___format__.patch'])

class CustomDevelopCommand(develop):

    def run(self):
        super(CustomDevelopCommand, self).run()
        print('Patching Pint')
        pint_loc = site.getsitepackages()[0]
        subprocess.call(['patch', '-s' , pint_loc + r'/pint/quantity.py',
                         r'patches/Fixed_unsupported_format_string_passed_to_numpy_ndarray___format__.patch'])

setup(
        name='MTIpython',
        version='0.2.dev',
        url='http://mti-gitlab.ihc.eu/generic-software/MTIpython',
        license='MIT License',
        author='Maarten in t Veld and Jelle Spijker',
        author_email='jm.intveld@ihcmti.com & j.spijker@ihcmti.com',
        description='MTI Python module for fluid, mechanical, thermodynamical and electrical '
                    'engineering',
        keywords='fluid, mechanical, thermodynamical, electrical',
        dependency_links=["http://www.coolprop.dreamhosters.com/binaries/Python/",
                          "git+https://github.com/hgrecco/pint.git@master#egg=Pint-0.9.dev0"],
        packages=find_packages(),
        install_requires=['coolprop>=6.1;python_version<="3.6"', 'dill', 'numpy', 'scipy',
                          'uncertainties', 'pyyaml', 'pint>=0.8.2'],
        include_package_data=True,
        cmdclass={
            'install': CustomInstallCommand,
            'develop': CustomDevelopCommand,
        },
)
