# MTIpython
A Python module which can be used for **fluid, mechanical, thermodynamical and electrical engineering**.  

It is recommeded to read the API documentation which can be reached under http://generic-software.pages.mti-gitlab.ihc.eu/MTIpython . 
Or it can be download as [pdf](http://mti-gitlab.ihc.eu/MTIpython/MTIpython/builds/artifacts/develop/raw/MTIpython.pdf?job=pdf).
Code coverage report can be found under http://generic-software.pages.mti-gitlab.ihc.eu/MTIpython/coverage.

![mtipint_example](https://gitlab.com/MTIpython/Documentation/raw/develop/resources/mtipint_example.gif)

## Installation
There are two types of installation procedures: User and developer.
The following prerequisites are needed:
* [Python](https://www.python.org/downloads/) _only tested against Python 3.6_
* [Git](https://git-scm.com/downloads) _when compiling from source_

### Normal user setup
A normal **Non**-development user has two ways of installing MTIpython. He can either choose to donwload the latest [wheel](http://mti-gitlab.ihc.eu/MTIpython/MTIpython/-/jobs/artifacts/develop/download?job=release) file.
extract the file __*.whl__ file to a known location and install MTIpython using the command prompt 
```cmd
pip install MTIpython-<version nr>-py3-none-any.whl
```

An other way is to compile MTIpython from source, pulling the code from the repository using git and issuing the following commands on the command prompt:
```cmd
git clone --recurse-submodules -j8 http://mti-gitlab.ihc.eu/MTIpython/MTIpython.git
cd MTIpython
git checkout master
pip install --trusted-host www.coolprop.dreamhosters.com --process-dependency-links --pre .
```

### Development setup
It's good practice to setup a [Virtual environment](https://virtualenv.pypa.io/en/stable/userguide/) for development usage. 
Enter the following command on the command prompt:

__IMPORTANT!__ Don't use `pip-sync` systemwide, make sure the virtual environment is activated!

```cmd
# Clone the project
git clone --recurse-submodules -j8 http://mti-gitlab.ihc.eu/MTIpython/MTIpython.git
cd MTIpython
git checkout develop

# Setup python to work with virtual environments
pip install virtualenv 
mkdir venv
virtualenv venv

# Activate the virtual environment for the current command promt
venv\Scripts\activate

# Install all needed python development packages
pip install --trusted-host www.coolprop.dreamhosters.com --process-dependency-links --pre .
pip install -r dev-requirements.txt
```

Don't hestitate to contact Jelle Spijker M: [j.spijker@ihcmti.com](mailto:j.spijker@ihcmti.com) T: +31880153027 for any questions regarding the installation.
